extends ParallaxBackground

onready var fg = $FG
onready var bg = $BG

onready var p1spawn = $P1Spawn
onready var p2spawn = $P2Spawn

var fg_data = null
var initial_fg_data = null

const TRANSPARENT = Color(0,0,0,0)


func _ready():
	
	initial_fg_data = fg.texture.get_data()
	_update_fg_data()


func _update_fg_data():
	
	if fg_data != null:
		fg_data.unlock()
	fg_data = fg.texture.get_data()
	fg_data.lock()


func fg_rewert():
	
	fg.texture.set_data(initial_fg_data)
	_update_fg_data()
	


func is_colliding(coords):
	
	for i in range(coords.size()):
		if coords[i].x < 1024 and coords[i].x > 0 and coords[i].y > 0 and coords[i].y < 600:
			if _is_not_transparent(coords[i]):
				return true
	
	return false

func _is_not_transparent(pos):

	return fg_data.get_pixelv(pos) != TRANSPARENT


func pixel_alpha_debug(pos):
	
	return fg_data.get_pixelv(pos)


func explosion(pos, radius):

	for x in range(-radius, radius + 1):
		for y in range(-radius, radius + 1):
			# Loop over a square shape
			if Vector2(x, y).length() > radius:
				# Filter out the corners to leave the circle in the middle
				continue
			var pixel = pos + Vector2(x,y) # Move the circle to `pos`
			if pixel.x < 0 or pixel.x >= fg_data.get_width():
				continue # Outside the map
			if pixel.y < 0 or pixel.y >= fg_data.get_height():
				continue # Outside the map
			fg_data.set_pixelv(pixel, TRANSPARENT) # Set pixel transparent
	
	fg_data.unlock()
	fg.texture.set_data(fg_data)
	fg_data.lock()
