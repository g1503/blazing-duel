extends Area2D

onready var animated_sprite = $Sprite
onready var jump_cd = $JumpCD
onready var aim_origin = $AimOrigin
onready var crosshair = $AimOrigin/Crosshair
onready var impulse_lvl1 = $AimOrigin/ImpulseLvL1
onready var impulse_lvl2 = $AimOrigin/ImpulseLvL2
onready var impulse_lvl3 = $AimOrigin/ImpulseLvL3
onready var aim_ui_animation = $AimOrigin/AnimationPlayer
onready var fireball = preload("res://scenes/FireBall.tscn")
onready var health_label = $HealthLabel
onready var footsteps_sfx = $Footsteps
onready var jump_sfx = $Jump

signal player_down(index)
signal player_hit(index)
signal player_failed_cast
signal spell_released

const GRAVITY = 9.8

enum States {Idle, Walk, Cast, Dead}

var state = States.Idle

export (float) var speed = 100
export (float) var jump_force = 200
export (int) var index = 1
export (float) var initial_cast_force = 150.0
export (float) var max_cast_force = 750.0
export (float) var min_cast_force = 250.0
export (float) var max_overcast = 450.0
export (int) var look_direction = 1

var his_turn = false
var cast_force = 0
var motion = Vector2(0,0)
var foot_pos = Vector2(0,0)
var on_floor = false
var can_jump = true
var can_cast = true
var health = 0
var default_health = 100

var casting = false
var height
var width
var spell

func _ready():
	
	var animation_frame = animated_sprite.frames.get_frame("idle",0)
	height = animation_frame.get_height() * scale.y
	width = animation_frame.get_width() * scale.x
	cast_force = initial_cast_force
	update_health_label()
	
# warning-ignore:return_value_discarded
	connect("player_down", get_parent(), "on_player_down")
# warning-ignore:return_value_discarded
	connect("player_failed_cast", get_parent(), "on_player_failed_cast")
# warning-ignore:return_value_discarded
	connect("player_hit", get_parent(), "on_player_hit")
# warning-ignore:return_value_discarded
	connect("spell_released", get_parent(), "on_spell_released")


func _physics_process(delta):
	
	if position.y >= 610:
		die()
	
	match state:
		0: animated_sprite.animation = "idle"
		1: animated_sprite.animation = "walk"
		2: animated_sprite.animation = "cast"
	
	if casting: 
		cast_force += cast_force * 0.5 * delta
		if cast_force >= min_cast_force and cast_force < max_cast_force/2:
			impulse_lvl1.show()
			spell.type = spell.Types.Weak
		elif cast_force >= max_cast_force/2 and cast_force < max_cast_force:
			impulse_lvl2.show()
			spell.type = spell.Types.Average
		elif cast_force >= max_cast_force and cast_force < max_cast_force + max_overcast:
			impulse_lvl3.show()
			spell.type = spell.Types.Strong
		
		if cast_force >= max_cast_force + max_overcast/2:
			aim_ui_animation.play("Overcast")
		
		if cast_force >= max_cast_force + max_overcast:
			spell.type = spell.Types.Overcasted
			failed_spell()
	
	if look_direction == -1 and !animated_sprite.flip_h:
		animated_sprite.flip_h = true
		aim_origin.rotation = PI
	elif look_direction == 1 and animated_sprite.flip_h:
		animated_sprite.flip_h = false
		aim_origin.rotation = 0
	
	foot_pos = position + Vector2(0, height/2)
	var foot_pos_arr = []
	for i in range(foot_pos.x-width/3, foot_pos.x+width/3+1):
		foot_pos_arr.append(Vector2(i,foot_pos.y))
	on_floor = collision_check(foot_pos_arr)
	
	if on_floor:
		motion.y = 0
		jump_cd.stop()
		can_jump = true
		if state == 1:
			if !footsteps_sfx.playing : footsteps_sfx.play()
		else:
			if footsteps_sfx.playing : footsteps_sfx.stop()
	elif state != 3:
		_apply_gravity()
	
	if his_turn and state != States.Dead:
		if not aim_origin.visible:
			aim_origin.show()
		listen_controls(delta)
	else:
		motion.x = 0
		if aim_origin.visible:
			if casting:
				reset_cast()
				spell.failed()
			aim_origin.hide()
	
	position += motion * delta


func collision_check(vector_arr):
	
	return get_parent().terrain.is_colliding(vector_arr)


func _apply_gravity():
	
	motion.y += GRAVITY


func listen_controls(delta):
	
	motion.x = 0
	
	if Input.is_action_just_pressed("jump") and can_jump:
		if footsteps_sfx.playing : footsteps_sfx.stop()
		jump_sfx.play()
		jump_cd.start()
		can_jump = false
		motion.y -= jump_force
	
	if can_cast and not casting and Input.is_action_just_pressed("cast"):
		casting = true
		can_cast = false
		cast_spell()
	
	if casting and Input.is_action_just_released("cast"):
		casting = false
		release_spell()
	
	var aim = int(Input.is_action_pressed("aim_up")) - int(Input.is_action_pressed("aim_down"))
	if not casting:
		aim_origin.rotation -= aim * 1.5 * delta * look_direction
	
	
	var direction = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	if direction != 0:
		if direction == 1 and position.x < 1020 || direction == -1 and position.x > 4:
			state = States.Walk
			look_direction = direction
			var obstacle_check_arr = []
			for i in range(foot_pos.y - height, foot_pos.y - height/3):
				obstacle_check_arr.append(Vector2(foot_pos.x + direction*width/3, i))
			var obstacle = collision_check(obstacle_check_arr)
			if !obstacle:
				motion.x = direction*speed
				var climb_check_arr = []
				for i in range(foot_pos.y - height/3, foot_pos.y):
					climb_check_arr.append(Vector2(foot_pos.x + direction*width/2, i))
				var can_climb = collision_check(climb_check_arr)
				if can_climb:
					position.y -= 1
	elif not casting:
		state = States.Idle


func cast_spell():
	
	state = States.Cast
	spell = fireball.instance()
	call_deferred("add_child", spell)
	spell.connect("freed", get_parent(), "on_spell_freed")
	spell.set_as_toplevel(true)
	spell.global_position = crosshair.global_position + Vector2(20,0).rotated(aim_origin.rotation)
	spell.motion = Vector2(4.5*cast_force,0).rotated(aim_origin.rotation)
	spell.terrain = get_parent().terrain


func release_spell():
	
	state = States.Idle
	if cast_force >= min_cast_force:
		spell.release()
		emit_signal("spell_released")
	else:
		failed_spell()
	
	reset_cast()

func failed_spell():
	
	casting = false
	if cast_force >= max_cast_force + max_overcast:
		spell.explode()
	else:
		spell.failed()
		emit_signal("player_failed_cast")
	
	reset_cast()


func reset_cast():
	
	casting = false
	
	state = States.Idle
	
	if aim_ui_animation.is_playing():
		aim_ui_animation.stop()
	impulse_lvl1.hide()
	impulse_lvl2.hide()
	impulse_lvl3.hide()
	
	cast_force = initial_cast_force


func _on_JumpCD_timeout():
	
	can_jump = true


func hit(damage):
	
	health -= damage
	if health <= 0:
		die()
	else:
		emit_signal("player_hit", index)
	update_health_label()


func die():
	if state != States.Dead:
		health = 0
		state = States.Dead
		update_health_label()
		emit_signal("player_down", index)

func update_health_label():
	
	health_label.text = str(health)


func ready_to_battle():
	
	can_cast = true
	state = States.Idle
	health = default_health
	update_health_label()
	show()
	health_label.show()


func turn_checker(player_index):
	
	if player_index == index:
		match his_turn:
			true:
				his_turn = false
			false:
				can_cast = true
				his_turn = true
