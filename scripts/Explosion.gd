extends Area2D

onready var lifespan = $LifeSpan
onready var animated_sprite = $AnimatedSprite

var target_scale = 0.75
var damage = 0

func _ready():
	
	scale = Vector2(target_scale,target_scale)
	lifespan.start()
	animated_sprite.play("default")


func _on_LifeSpan_timeout():
	
	queue_free()



func _on_Explosion_area_entered(area):
	
	if area.is_in_group("Characters"):
		area.hit(damage)
