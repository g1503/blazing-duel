extends Node2D

onready var terrain = $Terrain
onready var play_btn = $UI/Control/PlayBtn
onready var rounds_input = $UI/Control/RoundsInput
onready var turn_time_input = $UI/Control/TurnTimeInput
onready var turn_await_timer = $TurnAwaitTimer
onready var next_turn_await = $NextTurnAwaitTimer
onready var round_await_timer = $RoundAwaitTimer
onready var turn_time = $TurnTime
onready var health_input = $UI/Control/HealthInput
onready var anouncement = $UI/Control/AnouncementLabel
onready var player1 = $Character
onready var player2 = $Character2
onready var p1score_label = $UI/Control/P1Score
onready var p2score_label = $UI/Control/P2Score

var p1spawn = Vector2(0,0)
var p2spawn = Vector2(0,0)

var crnt_round = 0
var max_rounds = 2
var time_for_turn = 0
var points = [0,0]
var next_turn_player = 0
var turn_in_player = 0
var turn_active = false
var somebody_hit = false

func _ready():
	
	randomize()
	p1spawn = terrain.p1spawn.global_position
	p2spawn = terrain.p2spawn.global_position

func _process(_delta):
	
	if turn_active:
		anouncement.text = "Player %s has %s seconds for actions. Make history with your deeds!"%[turn_in_player, int(turn_time.time_left)]


func _on_PlayBtn_pressed():
	
	if crnt_round == 0:
		get_tree().call_group("Characters", "ready_to_battle")
		get_tree().call_group("PreGameUI", "hide")
		get_tree().call_group("InGameUI", "show")
		play_btn.hide()
		play_btn.text = "RESTART"
		match_start()
	else:
		get_tree().paused = false
		terrain.fg_rewert()
# warning-ignore:return_value_discarded
		get_tree().reload_current_scene()


func update_score():
	
	p1score_label.text = "P1 score: %s"%points[0]
	p2score_label.text = "P2 score: %s"%points[1]


func turn_stop():
	
	turn_active = false
	get_tree().call_group("Characters","turn_checker", turn_in_player)
	if !turn_time.is_stopped():
		turn_time.stop()


func turn_await():
	
	match turn_in_player:
		1: next_turn_player = 2
		2: next_turn_player = 1
	
	turn_await_timer.start()
	anouncement.text = "Round %s. Ready Player%s it is your turn."%[crnt_round, turn_in_player]


func turn_start():
	
	get_tree().call_group("Characters", "turn_checker", turn_in_player)
	turn_time.start()
	turn_active = true


func turn_end():
	
	turn_in_player = next_turn_player
	if somebody_hit:
		somebody_hit = false
	turn_await()


func round_start():

	crnt_round += 1
	
	player1.global_position = p1spawn
	player1.ready_to_battle()
	player2.global_position = p2spawn
	player2.ready_to_battle()
	
	turn_await()


func round_end():
	
	if crnt_round >= max_rounds:
		match_end()
	else:
		terrain.fg_rewert()
		round_start()

func match_start():
	
	var indexes = [1,2]
	turn_in_player = indexes[randi()%indexes.size()]
	
	if rounds_input.text.is_valid_integer():
		max_rounds = int(rounds_input.text)
	
	if turn_time_input.text.is_valid_integer():
		time_for_turn = int(turn_time_input.text)
	
	turn_time.wait_time = time_for_turn
	
	if health_input.text.is_valid_integer():
		var _health = int(health_input.text)
		player1.default_health = _health
		player2.default_health = _health
	
	round_start()


func match_end():
	
	play_btn.show()
	
	if points[0] > points[1]:
		anouncement.text = "Player ONE is best fire mage out there!"
	elif points[1] > points[0]:
		anouncement.text = "Player TWO is fire mage superior to anyone!"
	else:
		anouncement.text = "These two can't decide the winner. It's a draw."
	
	get_tree().paused = true


func on_spell_released():
	
	turn_stop()
	anouncement.text = "Fire in hole!"


func on_player_hit(index):
	
	somebody_hit = true
	if turn_active:
		turn_stop()
	if index == turn_in_player:
		anouncement.text = "Player%s played too much with fire."%turn_in_player
	else:
		anouncement.text = "Player%s hit his opponent. Well done!"%turn_in_player
	
	next_turn_await.start()


func on_player_down(index):
	
	somebody_hit = true
	if turn_active:
		turn_stop()
	match index:
		1: points[1] += 1
		2: points[0] += 1
	update_score()
	
	anouncement.text = "Player's %s mage departing to the better place."%index
	round_await_timer.start()


func on_player_failed_cast():
	
	turn_stop()
	anouncement.text = "Player%s failed to cast his spell..."%turn_in_player
	next_turn_await.start()


func on_spell_freed():
	
	yield(get_tree(), "idle_frame")
	if !somebody_hit:
		anouncement.text = "Player%s missed his opponent..."%turn_in_player
	next_turn_await.start()


func _on_TurnTime_timeout():
	
	turn_stop()
	anouncement.text = "Time is money. And Player%s is now bankrupt."%turn_in_player
	next_turn_await.start()


func _on_TurnAwaitTimer_timeout():
	
	turn_start()


func _on_NextTurnAwaitTimer_timeout():
	
	turn_end()


func _on_RoundAwaitTimer_timeout():
	
	round_end()
