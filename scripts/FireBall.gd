extends Area2D

signal freed

onready var animation = $AnimationPlayer
onready var explosion = preload("res://scenes/Explosion.tscn")
onready var cast_sfx = $CastSFX
onready var idle_sfx = $IdleSFX

enum Types {Weak, Average, Strong, Overcasted}

var type = Types.Strong

var motion = Vector2(0,0)
var released = false
var collided = false
var terrain

const damage_arr = [25,50,75,35]

func _ready():
	
	animation.play("Cast")
	cast_sfx.play()


func _physics_process(delta):
	
	if released and not collided:
		motion.y += 9.8
		position += motion * delta
		if terrain.is_colliding([global_position]):
			motion = Vector2(0,0)
			collided = true
			explode()
	
	if released: 
		if position.x > 1024 or position.x < 0 or position.y > 600:
			animation.play("Free")

func release():
	
	animation.play("Released")
	released = true
	cast_sfx.play()


func explode():
	
	idle_sfx.stop()
	animation.play("Free")
	
	var explosion_scale = 0.0
	
	match type:
		0: explosion_scale = 0.25
		1: explosion_scale = 0.5
		2: explosion_scale = 0.75
		3: explosion_scale = 1.5
	
	var explosion_area = explosion.instance()
	
	explosion_area.target_scale = explosion_scale
	
	explosion_area.damage = damage_arr[type]
	
	call_deferred("add_child", explosion_area)
	
	terrain.explosion(global_position, 64*explosion_scale)


func failed():
	
	animation.play("Failed")


func emit_freed():
	
	emit_signal("freed")


func _on_FireBall_area_entered(area):
	
	if area.is_in_group("Characters") and not collided:
		collided = true
		explode()
